<?php

$_['heading_title']			= 'Rest API';

$_['text_enabled']			= 'Включено';
$_['text_disabled']			= 'Отключено';
$_['text_feed']				= 'Feeds';
$_['text_success']			= 'Настройки сохранены';

$_['tab_general']			= 'Общее';
$_['text_edit']    			= 'Rest API настройки';
$_['entry_status']			= 'Статус:';
$_['entry_key']				= 'Секретный ключ:';
