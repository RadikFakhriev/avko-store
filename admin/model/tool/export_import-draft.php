<?php

protected function uploadProducts(&$reader, $incrimental, &$available_product_ids=array()) {

	$data = $reader->getSheetByName('Products');
	if ($data==null) {
		return;
	}


	// save product view counts
	$view_counts = $this->getProductViewCounts();

	// save old url_alias_ids
	$url_alias_ids = $this->getProductUrlAliasIds();

	// some older versions of OpenCart use the 'product_tag' table
	$query = $this->db->query( "SHOW TABLES LIKE '".DB_PREFIX."product_tag'" );
	$exist_table_product_tag = ($query->num_rows > 0);

	// Opencart versions from 2.0 onwards also have product_description.meta_title
	$sql = "SHOW COLUMNS FROM `".DB_PREFIX."product_description` LIKE 'meta_title'";
	$query = $this->db->query( $sql );
	$exist_meta_title = ($query->num_rows > 0) ? true : false;

	// if incremental then find current product IDs else delete all old products
	$available_product_ids = array();
	if ($incrimental) {
		$available_product_ids = $this->getAvailableProductIds($data);
	} else {
		$this->deleteProducts($exist_table_product_tag,$url_alias_ids);
	}


	// get pre-defined layouts
	$layout_ids = $this->getLayoutIds();

	// get pre-defined store_ids
	$available_store_ids = $this->getAvailableStoreIds();

	// find the installed languages
	$languages = $this->getLanguages();

	// find the default units
	$default_weight_unit = $this->getDefaultWeightUnit();
	$default_measurement_unit = $this->getDefaultMeasurementUnit();
	$default_stock_status_id = $this->config->get('config_stock_status_id');

	// find existing manufacturers, only newly specified manufacturers will be added
	$manufacturers = $this->getManufacturers();

	// get weight classes
	$weight_class_ids = $this->getWeightClassIds();

	// get length classes
	$length_class_ids = $this->getLengthClassIds();

	// get list of the field names, some are only available for certain OpenCart versions
	$query = $this->db->query( "DESCRIBE `".DB_PREFIX."product`" );
	$product_fields = array();
	foreach ($query->rows as $row) {
		$product_fields[] = $row['Field'];
	}

	// load the worksheet cells and store them to the database
	$first_row = array();
	$k = $data->getHighestRow();
	$language_code = "ru";
	for ($i = 0; $i < $k; $i+= 1) {
		if ($i==0) {
			$max_col = PHPExcel_Cell::columnIndexFromString( $data->getHighestColumn() );
			for ($j = 1; $j <= $max_col; $j+= 1) {
				$first_row[] = $this->getCell($data,$i,$j);
			}
			continue;
		}

		$j = 1;

		// ProductId проставляется в соответствии с номером строки
		$product_id = $i;

		// Excel: A1
		$category = array();
		while ($this->startsWith($first_row[$j-1], "Марка", false)) {
			$category_1st_level_name = htmlspecialchars( $this->getCell($data, $i, $j++));
			$category[$language_code]['brand'] = $category_1st_level_name;
		}

		// Excel: B1
		while ($this->startsWith($first_row[$j-1], "Модель", false)) {
			$category_2nd_level_name = htmlspecialchars( $this->getCell($data, $i, $j++));
			$category[$language_code]['car_model'] = $category_2nd_level_name;
		}
		$category_id = $this->findCategoryByName($category);

		// Excel: C1
		// Артикул. В терминах Opencart - Модель
		$model = $this->getCell($data, $i, $j++, '   ');

		//  Excel: D1
		// Тип товара. Поломана терминология OpenCart, "Производитель" имеет значение "Тип товара"
		$manufacturer_name = $this->getCell($data,$i,$j++);

		// Excel: E1
		$names = array();
		while ($this->startsWith($first_row[$j-1], "Наименование")) {
			$name = htmlspecialchars( $this->getCell($data, $i, $j++) );
			$names[$language_code] = $name;
		}

		// Excel: F1
		// Цена
		$price = $this->getCell($data, $i , $j++, '0.00');

		// Excel: G1
		$description_arr = array();
		while ($this->startsWith($first_row[$j-1], "Описание")) {
			$description = htmlspecialchars( $this->getCell($data, $i, $j++));
			$description_arr[$language_code] = $description;
		}


		// Product attributes
		$attribute_skirts_group_id = 7;
		$product_attributes_arr = array();


		// Excel: H1
		// product delivery set
		$delivery_set = array();
		while ($this->startsWith($first_row[$j-1], "Комплект поставки")) {
			$delivery_set['attribute_id'] = 17;
			$delivery_set['texts'][$language_code] = htmlspecialchars( $this->getCell($data, $i, $j++, ''));
		}
		$product_attributes_arr[] = $delivery_set;

		// Excel:  I1
		$product_material = array();
		while ($this->startsWith($first_row[$j-1], "Материал")) {
			$delivery_set['attribute_id'] = 13;
			$product_material['texts'][$language_code] = htmlspecialchars( $this->getCell($data, $i, $j++, ''));
		}
		$product_attributes_arr[] = $product_material;


		// Excel:  J1
		$tube_diametr = array();
		while ($this->startsWith($first_row[$j-1], "Диаметр трубы")) {
			$delivery_set['attribute_id'] = 14;
			$tube_diametr['texts'][$language_code] = htmlspecialchars( $this->getCell($data, $i, $j++, ''));
		}
		$product_attributes_arr[] = $tube_diametr;

		// Excel:  K1
		$sections_amount = array();
		while ($this->startsWith($first_row[$j-1], "Количество секций")) {
			$delivery_set['attribute_id'] = 15;
			$sections_amount['texts'][$language_code] = htmlspecialchars( $this->getCell($data, $i, $j++, ''));
		}
		$product_attributes_arr[] = $sections_amount;

		// Excel:  L1
		$shape = array();
		while ($this->startsWith($first_row[$j-1], "Форма")) {
			$delivery_set['attribute_id'] = 16;
			$shape['texts'][$language_code] = htmlspecialchars( $this->getCell($data, $i, $j++, ''));
		}
		$product_attributes_arr[] = $tube_diametr;

		$this->processAttributesGroup($product_id, $attribute_skirts_group_id, $product_attributes_arr);


		// Excel M1 - R1
		$main_image_path = $this->getCell($data,$i,$j++);
		$img_paths = array();
		for ($p = 0; $p < 5; $p+= 1) {
			$img_paths[] = $this->getCell($data,$i,$j++);
		}

		// Недостающие параметры для работы сохраняющих методов модуля
		$shipping = 'yes';
		$sku = '';
		$upc = '';
		$ean = '';
		$jan = '';
		$isbn = '';
		$mpn = '';
		$location = '';
		$quantity = '0';
		$points = '0';
		$date_added = 'NOW()';
		$date_modified = 'NOW()';
		$date_available = 'NOW()';
		$weight = '0';
		$weight_unit = $default_weight_unit;
		$length = '0';
		$width = '0';
		$height = '0';
		$measurement_unit = $default_measurement_unit;
		$status = 'true';
		$tax_class_id = '0';
		$keyword = '';
		$meta_titles = array();
		$meta_descriptions = array();
		$meta_keywords = array();
		$stock_status_id = $default_stock_status_id;
		$store_ids = array();
		$layout = array();
		$related = array();
		$tags = array();
		$sort_order = '0';
		$subtract = 'true';
		$minimum = '1';



		$product = array();
		$product['product_id'] = $product_id;
		$product['categories'] = ($category_id === "") ? array(): $category_id;
		$product['model'] = $model;
		$product['manufacturer_name'] = $manufacturer_name;
		$product['names'] = $names;
		$product['price'] = $price;
		$product['descriptions'] = $description_arr;
		$product['image'] = $main_image_path;

		// dummy
		$product['quantity'] = $quantity;
		$product['shipping'] = $shipping;
		$product['points'] = $points;
		$product['date_added'] = $date_added;
		$product['date_modified'] = $date_modified;
		$product['date_available'] = $date_available;
		$product['weight'] = $weight;
		$product['weight_unit'] = $weight_unit;
		$product['status'] = $status;
		$product['tax_class_id'] = $tax_class_id;
		$product['viewed'] = isset($view_counts[$product_id]) ? $view_counts[$product_id] : 0;
		$product['stock_status_id'] = $stock_status_id;
		$product['meta_titles'] = $meta_titles;
		$product['meta_descriptions'] = $meta_descriptions;
		$product['length'] = $length;
		$product['width'] = $width;
		$product['height'] = $height;
		$product['seo_keyword'] = $keyword;
		$product['measurement_unit'] = $measurement_unit;
		$product['sku'] = $sku;
		$product['upc'] = $upc;
		if (in_array('ean',$product_fields)) {
			$product['ean'] = $ean;
		}
		if (in_array('jan',$product_fields)) {
			$product['jan'] = $jan;
		}
		if (in_array('isbn',$product_fields)) {
			$product['isbn'] = $isbn;
		}
		if (in_array('mpn',$product_fields)) {
			$product['mpn'] = $mpn;
		}
		$product['location'] = $location;
		$product['store_ids'] = $store_ids;
		$product['subtract'] = $subtract;
		$product['minimum'] = $minimum;
		$product['meta_keywords'] = $meta_keywords;
		$product['tags'] = $tags;
		$product['sort_order'] = $sort_order;
		$product['related_ids'] = $related;
		$product['layout'] = $layout;



		$this->storeProductIntoDatabase( $product, $languages, $product_fields, $exist_table_product_tag, $exist_meta_title, $layout_ids, $available_store_ids, $manufacturers, $weight_class_ids, $length_class_ids, $url_alias_ids );

	}


}


protected function findCategoryByName($category_names) {

	$this->load->model('catalog/category');

	foreach ($category_names as $category_name_by_lang) {
		$category = $this->model_catalog_category->getCategories(array(
				'filter_name' => $category_name_by_lang['car_model']
		));
	}

	return (isset($category)) ? '' : $category[0]['category_id'];
}


protected function processAttributesGroup ($product_id, $attribute_group_id, $product_attributes_arr) {

	$product_attribute = array();

	$this->deleteProductAttribute( $product_id );
	$languages = $this->getLanguages();

	foreach ($product_attributes_arr as $attr_item) {
		$product_attribute['product_id'] = $product_id;
		$product_attribute['attribute_group_id'] = $attribute_group_id;
		$product_attribute['attribute_id'] = $attr_item['$attribute_id'];
		$product_attribute['texts'] = $attr_item['texts'];

		$this->storeProductAttributeIntoDatabase( $product_attribute, $languages );
	}
}
