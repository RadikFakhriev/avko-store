<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-gmap-geo-location" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-gmap-geo-location" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="gmap_geo_address_status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="gmap_geo_address_status" id="gmap_geo_address_status" class="form-control">
                                <?php if ($gmap_geo_address_status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php }
                                else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
<?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="gmap_geo_address_map_address"><?php echo $entry_map_address; ?></label>
                        <div class="col-sm-10">
                            <input type="text" id="gmap_geo_address_map_address" name="gmap_geo_address_map_address" placeholder="<?php echo $entry_map_address; ?>" value="<?php echo $gmap_geo_address_map_address; ?>"  class="form-control" />
                            <?php if ($error_map_address) { ?>
                                <div class="text-danger"><?php echo $error_map_address; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="gmap_geo_address_map_address"></label>
                        <div class="col-xs-10">
                            <div class="gmap-canvas"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Gmap Geo complete Plugin initialization-->
<script type="text/javascript">
    $(document).ready(function () {
        var options = {
            map: ".gmap-canvas",
            //location: 'Chennai, Tamil Nadu, India',
            mapOptions: {
                scrollwheel: true
              }
        };
        $("#gmap_geo_address_map_address").geocomplete(options)
                .bind("geocode:result", function (event, result) {
                    //console.log("Result: " + result.formatted_address);
                    var map = $("#gmap-address").geocomplete("map");
                    map.setZoom(12);
                    //console.log(map);
                })
                .bind("geocode:error", function (event, status) {
                    console.log("ERROR: " + status);
                })
                .bind("geocode:multiple", function (event, results) {
                    //console.log("Multiple: " + results.length + " results found");
                });
        $("#gmap_geo_address_map_address").trigger('geocode');
    });

</script>
<?php echo $footer; ?>
