<?php echo $header; ?><?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button type="submit" form="form-special" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-special" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                                <?php if ($error_name) { ?>
                                    <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-limit"><?php echo $entry_limit; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="limit" value="<?php echo $limit; ?>" placeholder="<?php echo $entry_limit; ?>" id="input-limit" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                            <div class="col-sm-10">
                                <select name="status" id="input-status" class="form-control">
                                    <?php if ($status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="active-discounts-list">
                            <?php foreach ($discounts as $dkey=>$discount) { ?>
                                <div id="discount-item-<?=$dkey?>" class="discount-item">
                                    <p class="discount-item__title">Акция №<?= $dkey+1?></p>
                                    <a onclick="confirm('Данное действие необратимо. Вы уверены?') ? removeDiscountItem(this) : false;" data-toggle="tooltip" title="" class="btn btn-danger del-discount" data-original-title="Удалить акцию"><i class="fa fa-minus-circle"></i></a>
                                    <div id="discount-entry-<?=$dkey?>">
                                        <div class="avko-admin-form-elem">
                                            <label class="col-sm-2 control-label">Название</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discounts[<?=$dkey?>][title]" placeholder="Название" value="<?=$discount['title']?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="avko-admin-form-elem">
                                            <label class="col-sm-2 control-label">Автомобиль</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discounts[<?=$dkey?>][categories]" value="" data-order="<?=$dkey?>" placeholder="Автомобиль" id="input-category-<?=$dkey?>" class="form-control categories-autocomplete" />
                                                <div id="categories-of-discount-<?=$dkey?>" class="well well-sm discount-categories-container" style="height: 150px; overflow: auto;">
                                                    <?php foreach ($discount['categories'] as $category_item) { ?>
                                                        <div id="category-of-discount<?= $category_item['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?= $category_item['name']; ?>
                                                            <input type="hidden" name="discounts[<?=$dkey?>][categories][]" value="<?= $category_item['category_id']; ?>" />
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="avko-admin-form-elem">
                                            <label class="col-sm-2 control-label">Текст акции</label>
                                            <div class="col-sm-10">
                                                <textarea name="discounts[<?=$dkey?>][text]" rows="5" class="form-control"><?=$discount['text']?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if(!count($discounts)) { ?>
                                <div id="discount-item-0" class="discount-item">
                                    <p class="discount-item__title">Акция №1</p>
                                    <a onclick="confirm('Данное действие необратимо. Вы уверены?') ? removeDiscountItem(this) : false;" data-toggle="tooltip" title="" class="btn btn-danger del-discount" data-original-title="Удалить акцию"><i class="fa fa-minus-circle"></i></a>
                                    <div id="discount-entry-0">
                                        <div class="avko-admin-form-elem">
                                            <label class="col-sm-2 control-label">Название</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discounts[0][title]" placeholder="Название" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="avko-admin-form-elem">
                                            <label class="col-sm-2 control-label">Автомобиль</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discounts[0][categories]" value="" data-order="0" placeholder="Автомобиль" id="input-category-0>" class="form-control categories-autocomplete" />
                                                <div id="categories-of-discount-0" class="well well-sm discount-categories-container" style="height: 150px; overflow: auto;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="avko-admin-form-elem">
                                            <label class="col-sm-2 control-label">Текст акции</label>
                                            <div class="col-sm-10">
                                                <textarea name="discounts[0][text]" rows="5" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <div id="add-discount-for-car" class="btn btn-primary" style="float: right; margin-right: 20px">Добавить акцию на модель автомобиля</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="view/javascript/handlebars/handlebars-v4.0.5.js"></script>
    <script src="view/javascript/handlebars/handlebars-helpers.js"></script>
    <script>
        var $addDiscountButton = $('#add-discount-for-car'),
            $discountsContainer = $('#active-discounts-list'),
            discountItemTmpl;

        $.get('view/template/html_partials/discount_item.hbs', function (templateMarkup) {
            discountItemTmpl = Handlebars.compile(templateMarkup);
        }, 'html');

        var catAutocompleteConfig = {
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item['name'],
                                value: item['category_id']
                            }
                        }));
                    }
                });
            },
            select: function(item) {
                var currentAutocomplete = $(this),
                    discountItemOrderNum = $(this).data('order');

                currentAutocomplete.val('');
                $('#categories-of-discount-' + discountItemOrderNum).find('#category-of-discount' + item['value']).remove();
                $('#categories-of-discount-' + discountItemOrderNum).append('<div id="category-of-discount' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="discounts[' + discountItemOrderNum + '][categories][]" value="' + item['value'] + '" /></div>');
            }
        };

        $('input.categories-autocomplete').autocomplete(catAutocompleteConfig);

        $('#active-discounts-list').delegate('.discount-categories-container .fa-minus-circle', 'click', function() {
            $(this).parent().remove();
        });

        $addDiscountButton.on('click', function (e) {
            var lastOrdinalNum = $('.discount-item').length,
                tmplData = { ordinalNum: lastOrdinalNum};

            $discountsContainer.append(discountItemTmpl(tmplData));
            $('#discount-item-' + lastOrdinalNum).find('input.categories-autocomplete').autocomplete(catAutocompleteConfig);
        });

        function removeDiscountItem(elem) {
            var $target = $(elem);
            $target.parents('.discount-item').remove();
        }
    </script>
    <style scoped>
        #active-discounts-list {
            border-top: 1px solid #bbb;
            margin-left: -15px;
            margin-right: -15px;
        }

        .avko-admin-form-elem {
            overflow: hidden;
            padding: 20px 0;
        }

        .discount-item {
            position: relative;
            padding: 20px 0;
        }

        .discount-item__title {
            padding-left: 50px;
            font-size: 16px;
        }

        .discount-item + .discount-item {
            border-top: 1px solid #ededed;
        }

        .del-discount {
            position: absolute;
            top: 15px;
            right: 15px;
        }


    </style>
<?php echo $footer; ?>