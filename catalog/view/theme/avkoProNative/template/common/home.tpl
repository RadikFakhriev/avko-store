<?php echo $header; ?>
<section class="avko-page-wrap avko-page--main">
    <?= $content_top ?>
    <div class="avko-page--main__section section our-benefits">
        <div class="section--red-overlay">
            <div class="our-benefits__content-wrapper avko-center-keeper">
                <h3 class="our-benefits__title section--red-overlay__title">Наши преимущества</h3>
                <p class="our-benefits__description">Узнайте пять причин, почему защиты, бампера и пороги стоит покупать именно у нас</p>
                <ul class="our-benefits__list benefits-list">
                    <li class="benefits-list__item">
                        <p class="benefit-icon tick-icon"></p>
                        <p class="benefit-sentence">Установлено более 3500 защит и порогов</p>
                    </li>
                    <li class="benefits-list__item">
                        <p class="benefit-icon search-list-icon"></p>
                        <p class="benefit-sentence">Более 2300 вариантов для всех современных автомобилей</p>
                    </li>
                    <li class="benefits-list__item">
                        <p class="benefit-icon tested-icon"></p>
                        <p class="benefit-sentence">Вся продукция протестирована на современном оборудовании</p>
                    </li>
                    <li class="benefits-list__item">
                        <p class="benefit-icon timer-icon"></p>
                        <p class="benefit-sentence">Установка за 20 минут в любом автосервисе России</p>
                    </li>
                    <li class="benefits-list__item">
                        <p class="benefit-icon percents-icon"></p>
                        <p class="benefit-sentence">Персональные скидки и различные варианты оплаты</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?= $content_bottom ?>
    <div id="DeliveryAnnounce" class="avko-page--main__section section--delivery-announce delivery-announce">
        <div class="delivery-announce__content">
            <h4 class="delivery-announce__title">Доставка в любую точку России</h4>
            <p class="delivery-announce__description">Мы доставим Ваш заказ в любую точку России, от Калининграда до Владивостока</p>

            <button data-bind="click: function(data, event) { callBackFormInvoke('USUAL', 0 ,data, event);}" class="avko-button--transparent-red-bordered delivery-announce__button">Узнать стоимость доставки</button>
        </div>
        <div class="section--hidden-container">
            <callback-modal-window params="keepInstance: getCallbackModalWindowViewModel "></callback-modal-window>
        </div>
    </div>
    <div class="avko-page--main__section section--red-bordered section--reviews reviews">
        <div class="section--red-bordered__header avko-center-keeper">
            <h3 class="section--red-bordered__title reviews__title">Отзывы</h3>
            <p class="section--red-bordered__description reviews__description">Узнайте, что говорят покупатели о наших товарах</p>
        </div>
        <div class="reviews__list reviews-list avko-center-keeper">
            <div class="reviews-list__item review-item">
                <img class="review-item__photo" src="images/reviews/maxim_car.png" height="177">
                <p class="review-item__text">
                    Товар отличного качества! И доставка очень порадовала,
                    буду советовать вас друзьям
                </p>
                <img class="review-item__author-photo" src="images/reviews/maxim.png">
                <p class="review-item__author-name">
                    Максим
                </p>
                    <span class="review-item__author-info">
                        г. Самара, BMW X3
                    </span>
            </div>
            <div class="reviews-list__item review-item">
                <img class="review-item__photo" src="images/reviews/diman_car.png" height="177">
                <p class="review-item__text">
                    Товар отличного качества! И доставка очень порадовала,
                    буду советовать вас друзьям
                </p>
                <img class="review-item__author-photo" src="images/reviews/diman.png">
                <p class="review-item__author-name">
                    Максим
                </p>
                    <span class="review-item__author-info">
                        г. Самара, BMW X3
                    </span>
            </div>
            <div class="reviews-list__item review-item">
                <img class="review-item__photo" src="images/reviews/vasily_car.png" height="177">
                <p class="review-item__text">
                    Товар отличного качества! И доставка очень порадовала,
                    буду советовать вас друзьям
                </p>
                <img class="review-item__author-photo" src="images/reviews/vasily.png">
                <p class="review-item__author-name">
                    Максим
                </p>
                    <span class="review-item__author-info">
                        г. Самара, BMW X3
                    </span>
            </div>
        </div>
    </div>
    <div class="avko-page--main__section section--red-bordered section--questions questions">
        <div class="section--red-bordered__header avko-center-keeper">
            <h3 class="section--red-bordered__title questions__title">У вас остались вопросы?</h3>
            <span class="questions__call-us">Позвоните нам:</span>
        </div>
        <p class="questions__phone-number avko-center-keeper"><?= $formatted_telephone;?></p>
        <span class="questions__note avko-center-keeper">бесплатный звонок с любого телефона</span>
    </div>
    <div class="avko-page--main__section section--gray section--leave-your-request leave-your-request">
        <p class="leave-your-request__offer avko-center-keeper">Или оставьте заявку и мы позвоним вам сами!</p>
        <form id="RequestForm" data-bind="submit: requestCall">
            <input type="text"
                   name="user-name"
                   class="request__user-name"
                   placeholder="Ваше имя"
                   data-bind="value: requestAuthorName, valueUpdate: 'keyup paste input'">
            <input type="tel"
                   name="phone-number"
                   class="request__phone-number phone-number"
                   placeholder="+7"
                   data-inputmask="'mask': '+7(999) 999-99-99'"
                   data-bind="value: requestAuthorPhone, valueUpdate: 'keyup paste input'">
            </br>
            <p class="validation-messages" data-bind="text: validateError"></p>
            <button type="submit" class="avko-button--red request__send-button">Отправить</button>
        </form>
    </div>
</section>
<script src="catalog/view/theme/avkoProNative/vendor/requirejs/require.js" data-main="catalog/view/theme/avkoProNative/assets/js/home.build.js"></script>
<?php echo $footer; ?>