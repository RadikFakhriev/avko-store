<footer class="avko-footer footer">
    <div class="avko-footer__section footer__section-address section-address">
        <table class="section-address__table footer-table avko-center-keeper">
            <tr>
                <td valign="top">
                    <b>Продукция</b>
                    <nav>
                        <ul class="footer__menu">
                            <?php foreach ($categories as $category) { ?>
                                <li>
                                    <a href="<?= $category['href']; ?>">
                                        <?= $category['name']; ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <li>
                                <a href="<?= $contact;?>">
                                    Контакты
                                </a>
                            </li>
                        </ul>
                    </nav>
                </td>
                <td width="300">
                    <b>Контакты</b>
                    <ul>
                        <li>
                            <i class="phone-icon"></i>Телефон: <?= $telephone;?>
                        </li>
                        <li>
                            <i class="mail-icon"></i>E-mail: <a href="mailto:<?= $email?>"><?= $email;?></a>
                        </li>
                        <li>
                            <i class="working-schedule-icon"></i><?= $open;?>
                        </li>
                    </ul>
                    <p class="b-address">
                        <?= $address;?>
                    </p>
                    <p class="b-requisites">
                        <?php
                            $str_arr = explode(";", $comment);
                            for ($i = 0; $i < count($str_arr); $i++) {
                                echo $str_arr[$i] . "<br/>";
                        }
                        ?>
                    </p>
                </td>
                <td valign="top" align="right">
                    <img width="470" src="catalog/view/theme/avkoProNative/assets/pictures/map.png">
                </td>
            </tr>
        </table>
    </div>
    <div class="avko-footer__section footer__section-copyright section-copyright avko-center-keeper">
        &copy; 2015 AvkoPro
    </div>
    <a href="#" id="back-to-top" title="Back to top">&#x25B2;</a>
</footer>
</body>
</html>