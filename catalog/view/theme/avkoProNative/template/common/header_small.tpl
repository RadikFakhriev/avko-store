<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <link rel="stylesheet" href="catalog/view/theme/avkoProNative/vendor/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="catalog/view/theme/avkoProNative/vendor/custombox/custombox.min.css">
    <link rel="stylesheet" href="catalog/view/theme/avkoProNative/vendor/jquery-ui/themes/base/all.css">
    <link rel="stylesheet" href="catalog/view/theme/avkoProNative/vendor/slick-carousel/slick-theme.css">
    <link rel="stylesheet" href="catalog/view/theme/avkoProNative/vendor/slick-carousel/slick.css">
    <link rel="stylesheet" href="catalog/view/theme/avkoProNative/assets/css/main.css">
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>">
<header class="avko-header">
    <div id="AppBar" class="avko-appbar">
        <div data-bind="toggleClick: menuIsOpen" class="avko-appbar__menu-button"></div>
        <menu class="avko-appbar__menu menu avko-center-keeper" data-bind="css: {'menu--open': menuIsOpen()}">
            <?php foreach ($categories as $category) { ?>
            <li>
                <a href="<?php echo $category['href']; ?>">
                    <?php echo $category['name']; ?>
                </a>
            </li>
            <?php } ?>
            <li>
                <a href="<?php echo $contact_link?>">
                    Контакты
                </a>
            </li>
        </menu>
        <div class="avko-appbar__overlay"
             data-bind="toggleClick: menuIsOpen,
                                css: {'appbar-overlay--inactive': !menuIsOpen()}">
        </div>
    </div>
    <div id="MainFeedback" class="avko-header__main-feedback main-feedback main-feedback--small">
        <div class="main-feedback__short-info short-info">
            <div class="avko-center-keeper">
                <a href="#" class="short-info__avko-logo avko-logo"></a>
                <div class="short-info__feedback-call feedback-call">
                    <div class="feedback-call__phone-block phone-block">
                        <div class="phone-block__number"><?= $formatted_telephone;?></div>
                        <div class="phone-block__text">бесплатный звонок по России</div>
                    </div>
                    <div data-bind="click:  function(data, event) { callBackFormInvoke('USUAL', 0, data, event);}" class="avko-button--light feedback-call__button">Обратный звонок</div>
                </div>
            </div>
        </div>
        <div class="section--hidden-container">
            <callback-modal-window params="keepInstance: getCallbackModalWindowViewModel "></callback-modal-window>
        </div>
    </div>
</header>