<div class="items-list__banner banner" style="display: none">
    <p class="banner__title"></p>
    <p class="banner__text"></p>
</div>
<style>
    .banner__text {
        padding-top: 20px;
    }
</style>
<script>
    var discountsList = <?=$discounts_list?>;

    for (dkey in discountsList) {
        var discount = discountsList[dkey];
        if (~discount.categories.indexOf(window.categoryId.toString())) {
            document.querySelector('.banner__title').innerHTML = discount.title;
            document.querySelector('.banner__text').innerHTML = discount.text;
            document.querySelector('.items-list__banner').style.display = "block";
        }
    }
</script>