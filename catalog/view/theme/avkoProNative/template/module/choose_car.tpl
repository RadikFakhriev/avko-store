<div id="ChooseBrand" class="avko-page--main__section section--red-bordered choose-brand">
    <div class="section--red-bordered__header avko-center-keeper">
        <h3 class="section--red-bordered__title choose-brand__title">Выбрать модель авто</h3>
        <p class="section--red-bordered__description choose-brand__description">Для выбора защиты и порога перейдите на страницу марки своего автомобиля</p>
    </div>
    <ul class="choose-brand__brands-grid brands-grid avko-center-keeper">
        <?php foreach ($categories as $category) { ?>
            <li class="brands-grid__cell auto-brand">
                <div class="auto-brand__item" data-brand-key="<?php echo $category['id']?>" data-bind="click:chooseModelFormInvoke">
                    <img src="<?php echo $category['image']; ?>">
                </div>
                <span class="auto-brand__title"><?php echo $category['name']?></span>
            </li>
        <?php } ?>
    </ul>
    <div class="section--hidden-container">
        <choose-model-modal-window params="keepInstance: getChooseModelViewModel"></choose-model-modal-window>
    </div>
</div>