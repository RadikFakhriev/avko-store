<div class="gmap-container">
    <div class="gmap-container__address gmap-address  avko-center-keeper">
        <address>
            <p><?php echo $gmap_geo_address; ?></p>
        </address>
        <h3 class="gmap-address__title"><?php echo $entry_map_geo_address; ?></h3>
    </div>

    <!--Gmap Autocomplete-->
    <form role="form">
        <!--Set Geo location value-->
        <input type="hidden" class="" id="gmap-address" placeholder="Gmap Address" value="<?php echo $gmap_geo_address; ?>">
        <!--/.Set Geo location value-->
        <div class="gmap-canvas"></div>
    </form>
    <!--/.Gmap Autocomplete-->
</div>
<script src="catalog/view/theme/avkoProNative/vendor/jquery/jquery.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="catalog/view/javascript/jquery/gmap-geocomplete/jquery.geocomplete.js"></script>
<!--Gmap Geocomplete Plugin-->
<script type="text/javascript">
    $(document).ready(function () {
        var options = {
            map: ".gmap-canvas",
            //location: 'Chennai, Tamil Nadu, India',
            mapOptions: {
                scrollwheel: true
              }
        };

        $("#gmap-address").geocomplete(options)
                .bind("geocode:result", function (event, result) {
                    //console.log("Result: " + result.formatted_address);
                    //var map = $("#gmap-address").geocomplete("map");
                    //map.setZoom(9);
                    //map.setCenter("Chennai, Tamil Nadu, India");
                    //console.log(map);
                })
                .bind("geocode:error", function (event, status) {
                    console.log("ERROR: " + status);
                })
                .bind("geocode:multiple", function (event, results) {
                    //console.log("Multiple: " + results.length + " results found");
                });
         $("#gmap-address").trigger("geocode");
    });
</script>