<div id="SalesLeaders" class="avko-page--main__section section--red-bordered section--sales-leaders sales-leaders">
    <div class="section--red-bordered__header avko-center-keeper">
        <h3 class="section--red-bordered__title sales-leaders__title">Лидеры продаж</h3>
        <p class="section--red-bordered__description sales-leaders__description">Выберите марку и модель вашего автомобиля</p>
    </div>
    <ul class="sales-leaders__list leaders-list avko-center-keeper">
        <?php foreach ($products as $product) { ?>
            <li class="leaders-list__cell leader-cell">
                <div class="leader-cell__inner-cell leader-cell__photo">
                    <a href="<?= $product['href'] ?>">
                        <img src="<?= $product['thumb'] ?>">
                    </a>
                </div>
                <div class="leader-cell__inner-cell leader-cell__preview-info preview-info-cell">
                    <p class="preview-info-cell__title"><?= $product['name']; ?></p>
                    <span class="preview-info-cell__designed-for-mark">для <?= $product['category']?></span>
                    <p class="preview-info-cell__price"> <?=  number_format(intval($product['price']), 0, '', ' ') ." Р"; ?></p>
                    <div data-bind="click:  function(data, event) { callBackFormInvoke('CONSULT', <?= $product['product_id']?>, data, event);}" class="leave-bid avko-button--red">Оставить заявку</div>
                    <a class="preview-info-cell__details" href="<?= $product['href'] ?>">подробнее</a>
                </div>
            </li>
        <?php } ?>
    </ul>
    <ul class="sales-leaders__list leaders-list avko-center-keeper sales-leaders__additional-products ">
        <!-- ko foreach: additionalLeaderProducts -->
            <li class="leaders-list__cell leader-cell">
                <div class="leader-cell__inner-cell leader-cell__photo">
                    <a data-bind="attr: { href: href} ">
                        <img data-bind="attr: { src: thumb}">
                    </a>
                </div>
                <div class="leader-cell__inner-cell leader-cell__preview-info preview-info-cell">
                    <p class="preview-info-cell__title" data-bind="text: name"></p>
                    <span class="preview-info-cell__designed-for-mark" data-bind="text: category">для </span>
                    <p class="preview-info-cell__price" data-bind="text: price"></p>
                    <div data-bind="click:  function(data, event) {  $root.callBackFormInvoke('CONSULT', $data.id, data, event); }" class="leave-bid avko-button--red">Оставить заявку</div>
                    <a class="preview-info-cell__details" data-bind="attr: { href: href}">подробнее</a>
                </div>
            </li>
        <!-- /ko -->
    </ul>
    <ul class="sales-leaders__additional-products-buffer">
        <?php foreach ($more_products as $addition_product) { ?>
            <li><?= $addition_product?></li>
        <?php } ?>
    </ul>
    <div data-bind="click: loadMoreLeaderProducts" class="sales-leaders__show-more avko-center-keeper show-more">
        <div class="show-more__icon"></div>
        <span>Показать ещё</span>
    </div>
    <div class="section--hidden-container">
        <callback-modal-window params="keepInstance: getCallbackModalWindowViewModel "></callback-modal-window>
    </div>
</div>