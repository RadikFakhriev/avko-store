<?php echo $header_small; ?>
<section class="avko-page-wrap avko-page--contact">
    <h1 class="avko-page--contact__title  avko-center-keeper"><?php echo $heading_title; ?></h1>
    <div class="avko-page--contact__store-info store-info avko-center-keeper">
        <ul class="store-info__address store-address">
            <li>
                <i class="phone-icon--red"></i>
                <?php echo $text_telephone; ?>:
                <?php echo $telephone; ?>
            </li>
            <li>
                <i class="mail-icon--red"></i>
                <?php echo $text_store_mail; ?>:
                <?php echo $store_mail; ?>
            </li>
            <li>
                <i class="working-schedule-icon--red"></i>
                <?php echo $text_open; ?>:
                <?php echo $open; ?>
            </li>
        </ul>
        <div class="store-info__additional">
            <?php
                $str_arr = explode(";", $comment);
                for ($i = 0; $i < count($str_arr); $i++) {
                    echo $str_arr[$i] . "<br/>";
            }
            ?>
        </div>
    </div>

    <?php echo $content_top; ?>
</section>
<script src="catalog/view/theme/avkoProNative/vendor/requirejs/require.js" data-main="catalog/view/theme/avkoProNative/assets/js/contact.build.js"></script>
<div class="avko-page--main__section section--red-bordered section--questions questions">
    <div class="section--red-bordered__header avko-center-keeper">
        <h3 class="section--red-bordered__title questions__title">У вас остались вопросы?</h3>
        <span class="questions__call-us">Позвоните нам:</span>
    </div>
    <p class="questions__phone-number avko-center-keeper">8-800-777-66-55</p>
    <span class="questions__note avko-center-keeper">бесплатный звонок с любого телефона</span>
</div>
<div class="avko-page--main__section section--gray section--leave-your-request leave-your-request">
    <p class="leave-your-request__offer avko-center-keeper">Или оставьте заявку и мы позвоним вам сами!</p>
    <form id="RequestForm" data-bind="submit: requestCall">
        <input type="text"
               name="user-name"
               class="request__user-name"
               placeholder="Ваше имя"
               data-bind="value: requestAuthorName, valueUpdate: 'keyup paste input'">
        <input type="tel"
               name="phone-number"
               class="request__phone-number phone-number"
               placeholder="+7"
               data-inputmask="'mask': '+7(999) 999-99-99'"
               data-bind="value: requestAuthorPhone, valueUpdate: 'keyup paste input'">
        </br>
        <p class="validation-messages" data-bind="text: validateError"></p>
        <button type="submit" class="avko-button--red request__send-button">Отправить</button>
    </form>
</div>
<?php echo $footer; ?>