<?php echo $header_small;
    $products_count = 0;?>
<script>
    window.categoryId = <?= $current_category?>;
</script>
<section class="avko-page-wrap avko-page--catalog">
    <div class="avko-page--catalog__items items-list">
        <div id="ChooseBrand" class="items-list__choose-model choose-model avko-center-keeper">
            <h2 class="choose-model__title"><?php echo $heading_title; ?></h2>
            <span class="choose-model__button">
                <i class="show-more-models"></i>
                <span>Выбрать другую модель</span>
            </span>
            <ul class="car-brands-list car-brands-list--hidden">
                <?php
                 foreach ($categories as $category) { ?>
                    <li class="car-brands-list__item" data-brand-key="<?php echo $category['id']?>" data-bind="click:chooseModelFormInvoke">
                        <a><?= $category['name']; ?></a>
                    </li>
                <?php } ?>
            </ul>
            <div class="section--hidden-container">
                <choose-model-modal-window params="keepInstance: getChooseModelViewModel"></choose-model-modal-window>
            </div>
        </div>
        <?php echo $content_top; ?>
        <div id="ItemsList" class="items-list__element">
            <h2></h2>
            <?php foreach ($products as $product) { ?>
                <div class="item catalog-item avko-center-keeper">
                    <div class="item__header first-column">
                        <h3 class="item__title">
                            <?= $product['name'];?>
                        </h3>
                        <p class="item__article"><?= ('Артикул '. $product['model']);?> </p>
                    </div>
                    <ul class="item__features">
                        <li>
                            <i class="icon-red-timer"></i>
                            <p class="feature-name">Установка 20 минут</p>
                        </li>
                        <li>
                            <i class="icon-guarantee"></i>
                            <p class="feature-name">Гарантия 10 лет</p>
                        </li>
                        <li>
                            <i class="icon-tick-in-circle"></i>
                            <p class="feature-name">Без сверления кузова</p>
                        </li>
                    </ul>
                    <div class="item__common-info">
                        <div class="item-common-info__billdoard billboard">
                            <div class="billboard__photos">
                                <div class="photos-slider-<?php echo $products_count; ?>  photos-slider">
                                    <div class="selected-photo">
                                        <div class="selected-photo__item">
                                            <img src="<?= $product['main_image']; ?>" alt="<?= $product['name']; ?>" title="<?= $product['name']; ?>" class="img-responsive" />
                                        </div>
                                        <?php if($product['has_discount']) { ?>
                                            <div class="selected-photo__disctount-marker discount-marker">Скидка</div>
                                        <?php } ?>
                                    </div>
                                    <div class="photos-preview">
                                        <div class="photos-preview__item">
                                            <img src="<?= $product['thumb']; ?>" alt="<?= $product['name']; ?>" title="<?= $product['name']; ?>" class="img-responsive" />
                                        </div>
                                        <?php foreach ($product['additional_images'] as $img_item) { ?>
                                            <div class="photos-preview__item">
                                                <img src="<?= $img_item['thumb']; ?>"/>
                                            </div>
                                        <?php } ?>
                                        <div class="slider-arrows"></div>
                                    </div>
                                    <div class="photos-slider__paths-buffer paths-buffer">
                                        <span class="paths-buffer__0"><?php echo $product['main_image'];?></span>
                                        <?php foreach ($product['additional_images'] as $key => $img_item) { ?>
                                            <span class="paths-buffer__<?= ++$key; ?>"><?= $img_item['image_big'];?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="billboard__buy-bar ">
                                <div class="buy-bar text--centered">
                                    <?php
                                        if ($product['has_discount']) {
                                    ?>
                                        <div class="buy-bar__old-price old-price">
                                            <?=  number_format(intval($product['price']), 0, '', ' '); ?>
                                            <div class="old-price__throwline"></div>
                                        </div>
                                        <p class="buy-bar__discount-price">
                                            <?=  number_format(intval($product['discount_price']), 0, '', ' ') ." Р"; ?>
                                        </p>
                                    <?php } else { ?>
                                        <p class="buy-bar__price">
                                            <?=  number_format(intval($product['price']), 0, '', ' ') ." Р"; ?>
                                        </p>
                                    <?php } ?>

                                    <span class="buy-bar__sales">
                                        <?php if($product['qty'] != '0') { ?>
                                            <label> Уже продано: </label>
                                             <?= $product['qty'] ?> компл.
                                        <?php }?>
                                    </span>
                                    <div data-bind="click:  function(data, event) { callBackFormInvoke('BUY', <?= $product['product_id']?>, data, event);}" class="avko-button--transparent-red-bordered buy-bar__button">Купить</div>
                                    <div data-bind="click:  function(data, event) { callBackFormInvoke('CONSULT', <?= $product['product_id']?>, data, event);}" class="avko-button--transparent-red-bordered buy-bar__button">Консультация</div>
                                    <div data-bind="click:  function(data, event) { callBackFormInvoke('CLAIM_DISCOUNT', <?= $product['product_id']?>, data, event);}" class="avko-button--transparent-red-bordered buy-bar__button">Хочу дешевле!</div>
                                    <div class="buy-bar__delivery-section delivery-section">
                                        <i class="delivery-section__icon"></i>
                                        <p class="delivery-section__info">
                                            Доставим в любую точку России
                                            <br/>
                                            <a data-bind="click:  function(data) { callBackFormInvoke('CLAIM_DISCOUNT', <?= $product['product_id']?>, data, event);}">узнать стоимость доставки</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item__text-content item-text-content item-text-content--hidden">
                            <div class="item-text-content__description">
                                <?php echo $product['description'];?>
                            </div>
                            <div class="item-text-content__product-props">
                                <p class="product-props__title">
                                    Характеристики
                                </p>
                                <div class="product-props__inner">
                                    <table class="product-props-table">
                                    <?php
                                    for ($i = 1; $i < sizeof($product['attributes']); $i++) {
                                        if(isset($product['attributes'][$i]['text'])) { ?>
                                            <tr>
                                                <td class="product-props-table__parameter"><?php echo $product['attributes'][$i]['name']?></td>
                                                <td>
                                                    <?= $product['attributes'][$i]['text']?>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    </table>
                                </div>
                            </div>
                            <div class="item-text-content__product-box product-box">
                                <p class="product-box__title"><?= $product['attributes'][0]['name'];?></p>
                                <ul class="product-box__inner">
                                    <?php
                                        $str_arr = explode(";", $product['attributes'][0]['text']);
                                        for ($i = 0; $i < count($str_arr); $i++) {
                                            echo "<li>" . $str_arr[$i] . "</li>";
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="item__toggle-more-details">
                            <div class="show-details">
                                <i>+</i>
                                <span>Показать характеристики</span>
                            </div>
                            <div class="hide-details">
                                <i>&ndash;</i>
                                <span>Скрыть характеристики</span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                $products_count++;
                if(sizeof($products) !== $products_count) {
             ?>
                <div class="items-list__separartor"></div>
            <?php }
            } ?>
            <div class="section--hidden-container">
                <callback-modal-window params="keepInstance: getCallbackModalWindowViewModel "></callback-modal-window>
            </div>
        </div>
    </div>

    <div class="avko-page--main__section section--red-bordered section--questions questions">
        <div class="section--red-bordered__header avko-center-keeper">
            <h3 class="section--red-bordered__title questions__title">У вас остались вопросы?</h3>
            <span class="questions__call-us">Позвоните нам:</span>
        </div>
        <p class="questions__phone-number avko-center-keeper"><?= $formatted_telephone;?></p>
        <span class="questions__note avko-center-keeper">бесплатный звонок с любого телефона</span>
    </div>
    <div class="avko-page--main__section section--gray section--leave-your-request leave-your-request">
        <p class="leave-your-request__offer avko-center-keeper">Или оставьте заявку и мы позвоним вам сами!</p>
        <form id="RequestForm" data-bind="submit: requestCall">
            <input type="text"
                   name="user-name"
                   class="request__user-name"
                   placeholder="Ваше имя"
                   data-bind="value: requestAuthorName, valueUpdate: 'keyup paste input'">
            <input type="tel"
                   name="phone-number"
                   class="request__phone-number phone-number"
                   placeholder="+7"
                   data-inputmask="'mask': '+7(999) 999-99-99'"
                   data-bind="value: requestAuthorPhone, valueUpdate: 'keyup paste input'">
            </br>
            <p class="validation-messages" data-bind="text: validateError"></p>
            <button type="submit" class="avko-button--red request__send-button">Отправить</button>
        </form>
    </div>
</section>
<script src="catalog/view/theme/avkoProNative/vendor/requirejs/require.js" data-main="catalog/view/theme/avkoProNative/assets/js/catalog.build.js"></script>
<?php echo $footer; ?>