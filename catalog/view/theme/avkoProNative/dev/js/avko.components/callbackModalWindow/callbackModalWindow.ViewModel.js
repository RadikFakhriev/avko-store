define([
    'jquery',
    'knockout',
    'custombox',
    'inputMasksHook',
    'sweetAlert'
], function ($, ko, custombox, inputMasksHook) {

    function callbackModalWindowVM (params) {
        var self = this,
            feedbackModalType,
            modalProductId,
            phoneNumberSub,
            nameSub,
            $name = $("input.callback__name"),
            $phone = $("input.callback__phone");

        self.phoneNumber = ko.observable(params.initialText || '');
        self.name = ko.observable(params.initialText || '');
        self.validateError = ko.observable(params.initialText || '');

        self.openModal = function (feedbackType, productId, targetElement) {
            var targetSelector = '',
                modalContainers = ['#MainFeedback', '#ItemsList', '#SalesLeaders', '#DeliveryAnnounce'];

            for (var p = 0; p < modalContainers.length; p++) {
                if($(targetElement).parents(modalContainers[p]).length) {
                    targetSelector = modalContainers[p];
                    break;
                }
            }
            targetSelector += ' .modal-window.callback';


            custombox.open({
                target: targetSelector,
                effect: 'slide',
                width: '490',
                escKey: true,
                position: ['center', 'center'],
                overlayColor: '#fff',
                overlayOpacity: 0.8
            });

            feedbackModalType = feedbackType;
            modalProductId = productId;
            inputMasksHook.init();
        };

        self.closeModal = function () {
            custombox.close();
        };

        self.callbackClaim = function () {
            var formData;

            phoneNumberSub ? phoneNumberSub.dispose() : null;
            nameSub ? nameSub.dispose() : null;

            if (validateForm()) {
                formData = {
                    enquiry: self.phoneNumber(),
                    email: 'info@avkopro.ru',
                    name: self.name(),
                    subject: 'Заявка на обратный звонок',
                    product:  null
                };

                switch (feedbackModalType) {
                    case 'USUAL':
                        formData.subject = 'Заявка на обратный звонок';
                        break;
                    case 'SPECIALIST_CONSULT':
                        formData.subject = 'Заявка на консультацию специалиста';
                        break;
                    case 'BUY':
                        formData.subject = 'Заявка на покупку товара';
                        break;
                    case 'CONSULT':
                        formData.subject = 'Заявка на консультацию по товару';
                        break;
                    case 'CLAIM_DISCOUNT':
                        formData.subject = 'Запрос на снижение цены товара';
                        break;
                }
                formData.product = modalProductId;

                $.ajax({
                    url: "index.php?route=feed/rest_api/requestFeedback&key=039",
                    dataType: "json",
                    type: "POST",
                    data : formData
                }).fail(function() {
                    self.closeModal();
                    setTimeout(function() {
                        swal("Заявка не отправлена", "Не удалось отправить запрос на обратный звонок", "error");
                    }, 0);
                }).done(function(response) {
                    self.closeModal();
                    setTimeout(function() {
                        if (response.hasOwnProperty("success")) {
                            (response.success) ? swal("Отправлено!", "Мы свяжемся в Вами в ближайшее время", "success") : swal("Заявка не отправлена", "Что-то пошло не так :(", "error");
                        }
                    }, 0);
                    self.phoneNumber(null);
                    self.name(null);
                });
            } else {
                self.validateError("Заполните поля корректными данными");
                phoneNumberSub = self.phoneNumber.subscribe(validateForm);
                nameSub = self.name.subscribe(validateForm);
            }
        };

        function validateForm () {
            var isSuccess = true,
                nameSnapIsValid = checkName(self.name()),
                mobileSnapIsValid = checkMobile(self.phoneNumber());

            if (!nameSnapIsValid) {
                $name.addClass('field-has-error');
                isSuccess = false;
            } else {
                $name.removeClass('field-has-error');
            }

            if (!mobileSnapIsValid) {
                $phone.addClass('field-has-error');
                isSuccess = false;
            } else {
                $phone.removeClass('field-has-error');
            }

            if (nameSnapIsValid && mobileSnapIsValid) {
                self.validateError("");
            } else {
                self.validateError("Заполните поля корректными данными");
            }

            return isSuccess;
        }

        function checkMobile(value) {
            var regexpMobile = /\+7\(\d{3}\).\d{3}-\d{2}-\d{2}/gmi;
            return value && (regexpMobile.test(value));
        }

        function checkName(value) {
            return value && value.length > 2 && value.length < 16;
        }

        params.keepInstance(self);
    }

    return callbackModalWindowVM;
});