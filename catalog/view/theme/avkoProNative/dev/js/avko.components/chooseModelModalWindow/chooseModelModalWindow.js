define([
    'knockout',
    '../avko.components/chooseModelModalWindow/chooseModelModalWindow.ViewModel',
    'text!../avko.components/chooseModelModalWindow/chooseModelModal.html',
], function (ko, chooseModelModalWindowVM, modalTpl) {

    function createWidget () {

        ko.components.register('choose-model-modal-window', {
            viewModel: chooseModelModalWindowVM,
            template: modalTpl
        });

    }


    return {
        createWidget: createWidget
    }
});