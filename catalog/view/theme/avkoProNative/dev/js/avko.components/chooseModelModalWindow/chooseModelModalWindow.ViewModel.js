define([
    'jquery',
    'knockout',
    'custombox',
    'knockout-jqueryui/selectmenu'
], function ($, ko, custombox) {

    function chooseModelModalWindowVM (params) {
        var self = this,
            recieveCarModelList,
            brandListIsInit = false;

        self.brandsList = ko.observableArray([]);
        self.carModelList = ko.observableArray([]);
        self.selectedCarModel = ko.observable();
        self.selectedBrand = ko.observable('');
        self.validationMessage = ko.observable('');

        self.openModal = function () {
            custombox.open({
                target: '#chooseModelModal',
                effect: 'fadein',
                width: '500',
                escKey: true,
                position: ['center', 'center'],
                overlayOpacity: 0.0001
            });
        };

        self.closeModal = function () {
            custombox.close();
        };

        self.setBrandsList = function (list) {
            self.brandsList(list);
            setTimeout(function() {
                brandListIsInit = !brandListIsInit;
            }, 0);

        };

        recieveCarModelList = function (id) {
            $.ajax({
                url: "index.php?route=feed/rest_api/getCarModels&brand=" + id + "&key=039",
                dataType: "json"
            }).done(function(response) {
                (response.success && response.hasOwnProperty('carModels')) ? self.carModelList(response.carModels) : null;
            });

        };

        self.selectedBrand.subscribe(function (newId) {
            self.validationMessage('');
            self.selectedCarModel(null);
            recieveCarModelList(newId);
        });


        self.goToCategory = function () {
            if (brandListIsInit) {
                if (self.selectedCarModel()) {
                    location = "index.php?route=product/category&path=" + self.selectedCarModel();
                } else {
                    self.validationMessage("Вы не выбрали интресующий Вас автомобиль");
                }
            }

        };

        params.keepInstance(self);
    }

    return chooseModelModalWindowVM;
});