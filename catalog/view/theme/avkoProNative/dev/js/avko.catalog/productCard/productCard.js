define([
    'jquery',
    'knockout',
    'slick'
], function () {

    function init() {

        // Слайдеры для всех товаров на странице
        $('.photos-slider').each(function (idx, item) {
            var carouselId = ".photos-slider-" + idx;

            $(carouselId + " .photos-preview").slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                slide: carouselId + " .photos-preview__item",
                //asNavFor: carouselId + ' .selected-photo',
                accessibility: true,
                arrows: true,
                responsive: true,
                centerPadding: 0,
                infinite: true,
                focusOnSelect: true,
                appendArrows: carouselId + " .slider-arrows",
                nextArrow: '<a class="slick-next"><i></i></a>',
                prevArrow: '<a class="slick-prev"><i></i></a>'

            });

            if($(carouselId + " .photos-preview__item").length <= 3) {
                $(carouselId).addClass("no-slide-photos");
            }

        });


        // Открыть подробное описание товара
        $('.items-list__element').on('click', '.item__toggle-more-details', function(e){
            var selfButton = $(this);
            selfButton.toggleClass('item__toggle-more-details--hide');
            selfButton.prev('.item-text-content').slideToggle();
        });

        var slideTmpl = '<div class="selected-photo__item"></div>';

        $('.photos-slider').on('beforeChange','.photos-preview.slick-slider', function(e, slick, currentSlide, nextSlide) {
            var imgPath = $(this).parent('.photos-slider').find('.paths-buffer__' + nextSlide).text(),
                $slide = $(slideTmpl).html($('<img src="' + imgPath + '">')).hide();

            $(this).prev(".selected-photo").find('.selected-photo__item').replaceWith($slide);
            $slide.fadeIn('slow');
        });

        $('.no-slide-photos').on('click','.photos-preview__item', function (e) {
            var nextSlide = $(this).index(),
                imgPath = $(this).parents('.photos-slider').find('.paths-buffer__' + nextSlide).text(),
                $slide = $(slideTmpl).html($('<img src="' + imgPath + '">')).hide();

            $(this).parents('.photos-slider').find('.selected-photo__item').replaceWith($slide);
            $slide.fadeIn('slow');
        });

    }

    return {
        init: init
    };

});