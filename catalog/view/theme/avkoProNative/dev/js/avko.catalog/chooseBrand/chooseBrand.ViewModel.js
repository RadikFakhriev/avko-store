define([

], function () {

    var ChooseBrandViewModel = function () {
        var chooseModelModalWindowInstance;

        this.chooseModelFormInvoke = function (data, event) {
            chooseModelModalWindowInstance.selectedBrand($(event.currentTarget).data('brand-key'));
            chooseModelModalWindowInstance.openModal();
        };

        this.getChooseModelViewModel = function (vm) {
            chooseModelModalWindowInstance = vm;
            var brandListBuffer = [];
            $('.car-brands-list__item').each(function (index, item) {
                var brandObj = {
                    id: $(item).data('brand-key'),
                    text: $(item).children('a').text()
                };
                brandListBuffer.push(brandObj);
            });
            chooseModelModalWindowInstance.setBrandsList(brandListBuffer);
        };

    };

    return ChooseBrandViewModel;
});