define([
    'knockout',
    'chooseModelModalWindow',
    'chooseBrand/chooseBrand.ViewModel'
], function (ko, chooseModelModalWindow, chooseBrandViewModel) {

    function init () {
        $('.choose-model__button').on('click', function (e) {
            $(this).next('.car-brands-list').slideToggle();
        });

        chooseModelModalWindow.createWidget();
        ko.applyBindings(new chooseBrandViewModel(), document.getElementById('ChooseBrand'));
    }

    return {
        init: init
    }
});