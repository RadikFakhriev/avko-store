define([

], function () {

    var catalogConfig = {
        subModules: {
            chooseBrand: {
                isEnable: true
            },
            productCard: {
                isEnable: true
            },
            requestForm: {
                isEnable: true
            }
        },

        common: {
            header: {
                isEnable: true
            },
            appbar: {
                isEnable: true
            },
            backToTop: {
                isEnable: true
            }
        }

    };

    return catalogConfig;
});
