define([

], function () {

    function CatalogViewModel() {
        var callBackModalWindowInstance,
            self = this;

        self.callBackFormInvoke = function (feedbackType, productId, data, event) {
            callBackModalWindowInstance.openModal(feedbackType, productId, event.target);
        };

        self.getCallbackModalWindowViewModel = function (vm) {
            callBackModalWindowInstance = vm;
        };

    };

    return CatalogViewModel;
});