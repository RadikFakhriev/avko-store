define([
    'jquery',
    'knockout',
    'inputMasksHook',
    'sweetAlert'
], function ($, ko,  inputMasksHook) {

    function requestFormVM() {
        var self = this,
            phoneNumberSub,
            nameSub,
            $name = $("input.request__user-name"),
            $phone = $("input.request__phone-number");

        self.requestAuthorPhone = ko.observable('');
        self.requestAuthorName = ko.observable('');
        self.validateError = ko.observable('');
        inputMasksHook.init();

        self.requestCall = function () {
            var formData;

            phoneNumberSub ? phoneNumberSub.dispose() : null;
            nameSub ? nameSub.dispose() : null;

            if (validateForm()) {
                formData = {
                    enquiry:  self.requestAuthorPhone(),
                    email: 'info@avkopro.ru',
                    name: self.requestAuthorName(),
                    subject: 'Заявка на обратный звонок'
                };

                $.ajax({
                    url: "index.php?route=feed/rest_api/requestFeedback&key=039",
                    dataType: "json",
                    type: "POST",
                    data : formData
                }).fail(function() {
                    swal("Заявка не отправлена", "Не удалось отправить запрос на обратный звонок", "error");
                }).done(function(response) {
                    if (response.hasOwnProperty("success")) {
                        (response.success) ? swal("Отправлено!", "Мы свяжемся в Вами в ближайшее время", "success") : swal("Заявка не отправлена", "Что-то пошло не так :(", "error");
                    }
                    self.requestAuthorPhone(null);
                    self.requestAuthorName(null);
                });
            } else {
                self.validateError("Заполните поля корректными данными");
                phoneNumberSub = self.requestAuthorPhone.subscribe(validateForm);
                nameSub = self.requestAuthorName.subscribe(validateForm);
            }
        };

        function validateForm () {
            var isSuccess = true,
                nameSnapIsValid = checkName(self.requestAuthorName()),
                mobileSnapIsValid = checkMobile(self.requestAuthorPhone());

            if (!nameSnapIsValid) {
                $name.addClass('field-has-error');
                isSuccess = false;
            } else {
                $name.removeClass('field-has-error');
            }

            if (!mobileSnapIsValid) {
                $phone.addClass('field-has-error');
                isSuccess = false;
            } else {
                $phone.removeClass('field-has-error');
            }

            if (nameSnapIsValid && mobileSnapIsValid) {
                self.validateError("");
            } else {
                self.validateError("Заполните поля корректными данными");
            }

            return isSuccess;
        }

        function checkMobile(value) {
            var regexpMobile = /\+7\(\d{3}\).\d{3}-\d{2}-\d{2}/gmi;
            return value && (regexpMobile.test(value));
        }

        function checkName(value) {
            return value && value.length > 2 && value.length < 16;
        }

    };

    return requestFormVM;
});