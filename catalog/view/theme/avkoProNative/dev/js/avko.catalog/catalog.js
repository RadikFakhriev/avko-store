define([
    'require',
    'catalog.config',
    'knockout',
    '../avko.catalog/catalog.ViewModel',
    'header',
    'appbar',
    'backToTop',
    'requestForm/requestForm',
    'productCard/productCard',
    'chooseBrand/chooseBrand'
], function (require, config, ko, catalogViewModel) {

    function init () {

        Object.keys(config.subModules).forEach(function (moduleName) {
            (function (item) {
                var currentModuleStr = item + '/' + item,
                    module = require(currentModuleStr);

                if (config.subModules[item].isEnable) {
                    module.init();
                }
            })(moduleName);
        });

        Object.keys(config.common).forEach(function (moduleName) {
            (function (item) {
                var currentModuleStr = item ,
                    module = require(currentModuleStr);

                if (config.common[item].isEnable) {
                    module.init();
                }
            })(moduleName);
        });

        ko.applyBindings(new catalogViewModel(), document.getElementById('ItemsList'));

    }

    return {
        init: init
    };
});