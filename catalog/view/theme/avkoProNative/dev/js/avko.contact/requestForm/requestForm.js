define([
    "knockout",
    "requestForm/requestForm.ViewModel"
], function (ko, requestFormViewModel) {

    function init () {
        ko.applyBindings(new requestFormViewModel(), document.getElementById('RequestForm'));
    }

    return {
        init: init
    }
});