define([

], function () {

    var catalogConfig = {
        subModules: {
            requestForm: {
                isEnable: true
            }
        },

        common: {
            header: {
                isEnable: true
            },
            appbar: {
                isEnable: true
            },
            backToTop: {
                isEnable: true
            }
        }

    };

    return catalogConfig;
});
