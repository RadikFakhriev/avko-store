require.config({

    paths: {
        domReady: '../../../vendor/requirejs-domready/domReady',
        text: '../../../vendor/text/text',
        jquery: '../../../vendor/jquery/jquery',
        knockout: '../../../vendor/knockout/knockout',
        custombox: '../../../vendor/custombox/custombox.min',
        maskedInput: '../../../vendor/jquery.inputmask/jquery.inputmask.bundle',
        'jquery-ui': '../../../vendor/jquery-ui/ui',
        'knockout-jqueryui': '../../../vendor/knockout-jqueryui/dist/amd',
        inputMasksHook: '../avko.common/inputMasks/inputMasks',
        sweetAlert: '../../../vendor/sweetalert/sweetalert.min',
        appbar: '../avko.common/appbar/appbar',
        header: '../avko.common/header/header',
        backToTop: '../avko.common/backToTop/backToTop',
        callbackModalWindow: '../avko.components/callbackModalWindow/callbackModalWindow',
        chooseModelModalWindow: '../avko.components/chooseModelModalWindow/chooseModelModalWindow'
    },

    shim: {
        jquery: {
            exports: 'jquery'
        },
        knockout: {
            exports: 'knockout'
        },
        custombox: {
            exports: 'custombox',
            deps: ['jquery']
        },
        maskedInput: {
            exports: 'maskedInput',
            deps: ['jquery']
        },
        'knockout-jqueryui': {
            exports: 'knockout-jqueryui',
            deps: ['jquery', 'knockout']
        }
    },

    priority: ['knockout'],
    waitSeconds: 20
});


require(['knockout', 'contact', 'domReady!'], function(ko, appCatalog) {
    appCatalog.init();
});