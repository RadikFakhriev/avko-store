define([
    'require',
    'home.config',
    'knockout',
    'salesLeaders/salesLeaders.ViewModel',
    'deliveryAnnounce/deliveryAnnounce.ViewModel',
    'salesLeaders/salesLeaders',
    'reviews/reviews',
    'requestForm/requestForm',
    'chooseBrand/chooseBrand',
    'header',
    'appbar',
    'backToTop'
], function (require, config, ko, salesLeadersViewModel, deliveryAnnounceViewModel) {

    function init () {

        Object.keys(config.subModules).forEach(function (moduleName) {
            (function (item) {
                var currentModuleStr = item + '/' + item,
                    module = require(currentModuleStr);

                if (config.subModules[item].isEnable) {
                    module.init();
                }
            })(moduleName);
        });

        Object.keys(config.common).forEach(function (moduleName) {
            (function (item) {
                var currentModuleStr = item ,
                    module = require(currentModuleStr);

                if (config.common[item].isEnable) {
                    module.init();
                }
            })(moduleName);
        });

        ko.applyBindings(new salesLeadersViewModel(), document.getElementById('SalesLeaders'));
        ko.applyBindings(new deliveryAnnounceViewModel(), document.getElementById('DeliveryAnnounce'));
    }

    return {
        init: init
    };
});