define([
    'knockout',
    'jquery'
], function (ko, $) {



    function SalesLeadersViewModel() {
        var callBackModalWindowInstance,
            self = this;

        self.additionalLeaderProducts = ko.observableArray([]);

        self.callBackFormInvoke = function (feedbackType, productId, data, event) {
            callBackModalWindowInstance.openModal(feedbackType, productId, event.target);
        };

        self.getCallbackModalWindowViewModel = function (vm) {
            callBackModalWindowInstance = vm;
        };

        self.loadMoreLeaderProducts = function (){
            var productIds = $('.sales-leaders__additional-products-buffer li').map(function(idx, elem) {
               return parseInt($(this).text());
            }).toArray();

            var formData = {
                products: productIds
            };

            $.ajax({
                url: "index.php?route=feed/rest_api/getProductsByIds&key=039",
                dataType: "json",
                type: "POST",
                data : formData
            }).done(function(response) {
                if (response.success && response.hasOwnProperty('products')) {
                    self.additionalLeaderProducts(response.products);

                    $('.sales-leaders__additional-products').slideDown(1200).promise().done(function(){
                        $('.sales-leaders__show-more').hide();
                    });
                }
            });
        };

    };

    return SalesLeadersViewModel;
});