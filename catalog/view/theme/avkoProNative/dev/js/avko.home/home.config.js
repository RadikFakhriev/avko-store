define([

], function () {

    var homeConfig = {
        subModules: {
            chooseBrand: {
                isEnable: true
            },
            requestForm: {
                isEnable: true
            },
            reviews: {
                isEnable: true
            },
            salesLeaders: {
                isEnable: true
            }
        },

        common: {
            header: {
                isEnable: true
            },
            appbar: {
                isEnable: true
            },
            backToTop: {
                isEnable: true
            }
        }

    };

    return homeConfig;
});
