define([

], function () {

    var ChooseBrandViewModel = function () {
        var chooseModelModalWindowInstance;

        this.chooseModelFormInvoke = function (data, event) {
            chooseModelModalWindowInstance.selectedBrand($(event.currentTarget).data('brand-key'));
            chooseModelModalWindowInstance.openModal();
        };

        this.getChooseModelViewModel = function (vm) {
            chooseModelModalWindowInstance = vm;
            var brandListBuffer = [];
            $('.brands-grid__cell').each(function (index, item) {
                var brandObj = {
                    id: $(item).children('.auto-brand__item').data('brand-key'),
                    text: $(item).children('.auto-brand__title').text()
                };
                brandListBuffer.push(brandObj);
            });
            chooseModelModalWindowInstance.setBrandsList(brandListBuffer);
        };

    };

    return ChooseBrandViewModel;
});