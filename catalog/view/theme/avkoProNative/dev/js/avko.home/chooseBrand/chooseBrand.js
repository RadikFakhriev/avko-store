define([
    'knockout',
    'chooseModelModalWindow',
    'chooseBrand/chooseBrand.ViewModel'
], function (ko, chooseModelModalWindow, chooseBrandViewModel) {

    function init () {
        chooseModelModalWindow.createWidget();
        ko.applyBindings(new chooseBrandViewModel(), document.getElementById('ChooseBrand'));
    }

    return {
        init: init
    }
});