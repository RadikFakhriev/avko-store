require.config({

    paths: {
        domReady: '../../../vendor/requirejs-domready/domReady',
        text: '../../../vendor/text/text',
        jquery: '../../../vendor/jquery/jquery',
        knockout: '../../../vendor/knockout/knockout',
        slick: '../../../vendor/slick-carousel/slick.min',
        custombox: '../../../vendor/custombox/custombox.min',
        maskedInput: '../../../vendor/jquery.inputmask/jquery.inputmask.bundle',
        'jquery-ui': '../../../vendor/jquery-ui/ui',
        'knockout-jqueryui': '../../../vendor/knockout-jqueryui/dist/amd',
        sly: '../../../vendor/sly/sly.min',
        sweetAlert: '../../../vendor/sweetalert/sweetalert.min',
        inputMasksHook: '../avko.common/inputMasks/inputMasks',
        appbar: '../avko.common/appbar/appbar',
        header: '../avko.common/header/header',
        backToTop: '../avko.common/backToTop/backToTop',
        callbackModalWindow: '../avko.components/callbackModalWindow/callbackModalWindow',
        chooseModelModalWindow: '../avko.components/chooseModelModalWindow/chooseModelModalWindow'
    },

    shim: {
        jquery: {
            exports: 'jquery'
        },
        knockout: {
            exports: 'knockout'
        },
        slick: {
            exports: 'slick',
            deps: ['jquery']
        },
        custombox: {
            exports: 'custombox',
            deps: ['jquery']
        },
        maskedInput: {
            exports: 'maskedInput',
            deps: ['jquery']
        },
        'knockout-jqueryui': {
            exports: 'knockout-jqueryui',
            deps: ['jquery', 'knockout']
        },
        sly: {
            exports: 'sly',
            deps: ['jquery']
        },
        sweetAlert: {
            exports: 'sweetAlert'
        }
    },

    priority: ['knockout'],
    waitSeconds: 20
});


require(['knockout', 'home', 'domReady!'], function (ko, appHome) {
    appHome.init();
});