define([
    'jquery',
    'maskedInput'
], function ($, maskedInput) {

    function init () {
        $('input[type="tel"]').inputmask({ mask: "+7(999)999-99-99"});
    }

    return {
        init: init
    }

});