define([
    'knockout',
    '../avko.common/appbar/appbar.ViewModel',
    '../avko.components/toggleClick/onClickToggler'
], function (ko, appbarViewModel) {

    function init () {
        ko.applyBindings(new appbarViewModel(), document.getElementById('AppBar'));
    }

    return {
        init: init
    }
});