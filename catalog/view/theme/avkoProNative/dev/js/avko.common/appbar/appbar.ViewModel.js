define([
    'knockout'
], function (ko) {
   
    function appBarViewModel() {
        var self = this;

        self.menuIsOpen = ko.observable(false);
    }


    return appBarViewModel;
});