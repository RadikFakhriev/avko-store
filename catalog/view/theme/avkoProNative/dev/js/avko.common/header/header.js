define([
    'knockout',
    'callbackModalWindow',
    '../avko.common/header/header.ViewModel'
], function (ko, callbackModalWindow, headerViewModel) {

    function init () {
        callbackModalWindow.createWidget();
        ko.applyBindings(new headerViewModel(), document.getElementById('MainFeedback'));
    }

    return {
        init: init
    }

});
