var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    nib = require('nib'),
    rimraf = require('gulp-rimraf'),
    concat = require('gulp-concat'),
    spritesmith = require("gulp.spritesmith"),
    mainBowerFiles = require('main-bower-files'),
    rjs = require('gulp-requirejs'),
    print = require("gulp-print"),
    rename = require("gulp-rename"),
    autoPrefixer = require('autoprefixer-stylus');


gulp.task('stylus', function () {
    rimraf('./assets/css');
    gulp.src(['./dev/**/*.styl', './dev/*.styl'])
        .pipe(stylus({error: true, use: [nib(), autoPrefixer()]}))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('js', function () {
    rjs({
        baseUrl: './dev/js/avko.catalog',
        out: 'catalog.build.js',
        generateSourceMaps: true,
        preserveLicenseComments: false,
        optimize: 'uglify2',
        paths: {
            domReady: '../../../vendor/requirejs-domready/domReady',
            text: '../../../vendor/text/text',
            jquery: '../../../vendor/jquery/jquery',
            knockout: '../../../vendor/knockout/knockout',
            slick: '../../../vendor/slick-carousel/slick.min',
            custombox: '../../../vendor/custombox/custombox.min',
            maskedInput: '../../../vendor/jquery.inputmask/jquery.inputmask.bundle',
            'jquery-ui': '../../../vendor/jquery-ui/ui',
            'knockout-jqueryui': '../../../vendor/knockout-jqueryui/dist/amd',
            sweetAlert: '../../../vendor/sweetalert/sweetalert.min',
            inputMasksHook: '../avko.common/inputMasks/inputMasks',
            appbar: '../avko.common/appbar/appbar',
            header: '../avko.common/header/header',
            backToTop: '../avko.common/backToTop/backToTop',
            callbackModalWindow: '../avko.components/callbackModalWindow/callbackModalWindow',
            chooseModelModalWindow: '../avko.components/chooseModelModalWindow/chooseModelModalWindow'
        },
        name: 'app.catalog',
        waitSeconds: 20
    })
    .pipe(gulp.dest('./assets/js/'));



    rjs({
        baseUrl: './dev/js/avko.home',
        out: 'home.build.js',
        generateSourceMaps: true,
        preserveLicenseComments: false,
        optimize: 'uglify2',
        paths: {
            domReady: '../../../vendor/requirejs-domready/domReady',
            text: '../../../vendor/text/text',
            jquery: '../../../vendor/jquery/jquery',
            knockout: '../../../vendor/knockout/knockout',
            slick: '../../../vendor/slick-carousel/slick.min',
            custombox: '../../../vendor/custombox/custombox.min',
            maskedInput: '../../../vendor/jquery.inputmask/jquery.inputmask.bundle',
            'jquery-ui': '../../../vendor/jquery-ui/ui',
            'knockout-jqueryui': '../../../vendor/knockout-jqueryui/dist/amd',
            sly: '../../../vendor/sly/sly.min',
            sweetAlert: '../../../vendor/sweetalert/sweetalert.min',
            inputMasksHook: '../avko.common/inputMasks/inputMasks',
            appbar: '../avko.common/appbar/appbar',
            header: '../avko.common/header/header',
            backToTop: '../avko.common/backToTop/backToTop',
            callbackModalWindow: '../avko.components/callbackModalWindow/callbackModalWindow',
            chooseModelModalWindow: '../avko.components/chooseModelModalWindow/chooseModelModalWindow'
        },
        name: 'app.home',
        waitSeconds: 20
    })
    .pipe(gulp.dest('./assets/js/'));


    rjs({
        baseUrl: './dev/js/avko.contact',
        out: 'contact.build.js',
        generateSourceMaps: true,
        preserveLicenseComments: false,
        optimize: 'uglify2',
        paths: {
            domReady: '../../../vendor/requirejs-domready/domReady',
            text: '../../../vendor/text/text',
            jquery: '../../../vendor/jquery/jquery',
            knockout: '../../../vendor/knockout/knockout',
            custombox: '../../../vendor/custombox/custombox.min',
            maskedInput: '../../../vendor/jquery.inputmask/jquery.inputmask.bundle',
            'jquery-ui': '../../../vendor/jquery-ui/ui',
            'knockout-jqueryui': '../../../vendor/knockout-jqueryui/dist/amd',
            sweetAlert: '../../../vendor/sweetalert/sweetalert.min',
            inputMasksHook: '../avko.common/inputMasks/inputMasks',
            appbar: '../avko.common/appbar/appbar',
            header: '../avko.common/header/header',
            backToTop: '../avko.common/backToTop/backToTop',
            callbackModalWindow: '../avko.components/callbackModalWindow/callbackModalWindow',
            chooseModelModalWindow: '../avko.components/chooseModelModalWindow/chooseModelModalWindow'
        },
        name: 'app.contact',
        waitSeconds: 20
    })
    .pipe(gulp.dest('./assets/js/'));
});

gulp.task('html', function () {
    gulp.src(['./dev/*.html'])
        .pipe(gulp.dest('./assets/'));
});

gulp.task("bower-files", function(){
    var currentDirBuffer = '',
        idxClosePathDir,
        overrideLibsMainFilesConfig = {
            sly: {
                main: [
                    './dist/sly.min.js'
                ]
            },
            'requirejs-domready' : {
                main: [
                    'domReady.js'
                ]
            },
            'jquery-migrate': {
                main: [
                    'jquery-migrate.min.js'
                ]
            },
            'slick-carousel': {
                main: [
                    "slick/slick.min.js",
                    "slick/slick.css",
                    "slick/slick-theme.css",
                    "slick/ajax-loader.gif",
                    "slick/fonts/*"
                ]
            }
        };

    //2 pipe for divide libs on folders
    gulp.src(mainBowerFiles({
            overrides: overrideLibsMainFilesConfig
        }))
        .pipe(print(function (filepath) {
            //remove 'bower_components\' substring
            currentDirBuffer = filepath.substr(22);
            idxClosePathDir = currentDirBuffer.indexOf("\\");
            currentDirBuffer = currentDirBuffer.substr(0, idxClosePathDir);
        }))
        .pipe(rename(function (filepath) {
            filepath.dirname = currentDirBuffer;
        }))
        .pipe(gulp.dest("./vendor/"));
});



gulp.task('images', function () {
    var spriteData =
        gulp.src('./dev/img/*.png')
            .pipe(spritesmith({
                imgName: 'sprite.png',
                imgPath: '../img/sprite.png',
                cssName: 'stylus/avko.common/sprite.styl',
                cssFormat: 'stylus',
                algorithm: 'binary-tree',
                cssVarMap: function(sprite) {
                    sprite.name = 'sprite-' + sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest('./assets/img/'));
    spriteData.css.pipe(gulp.dest('./dev/'));
});

gulp.task('pics', function () {
    gulp.src(['./dev/pictures/**'])
        .pipe(gulp.dest('./assets/pictures'));
});

gulp.task('watch', function () {
    var initiallyTasks = ['pics', 'images', 'html', 'stylus', 'js'];

    gulp.start(initiallyTasks);

    gulp.watch('./dev/**/*.png',['images']);
    gulp.watch(['./dev/**/*.styl', './dev/*.styl'], ['stylus']);
    gulp.watch('./dev/**/*.html', ['html']);
    gulp.watch('./dev/**/*.js', ['js']);
});

gulp.task('build', function () {
    rimraf('./assets/css');
    rimraf('./assets/img');
    rimraf('./assets/js');
    rimraf('./assets/pictures');
    gulp.start(['images', 'pics', 'html', 'stylus', 'js']);
});

