<?php
class ModelToolImage extends Model {
	public function resize($filename, $width, $height, $excludeWhiteSpace = true) {

		if (!is_file(DIR_IMAGE . $filename)) {
			return null;
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$old_image = $filename;
		$new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
			$path = '';

			$directories = explode('/', dirname(str_replace('../', '', $new_image)));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}
			}

			list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_IMAGE . $old_image);
                // Сделано ради исключения белого пространства на изображениях
                if ($excludeWhiteSpace) {
                    if ($width/$height > $width_orig/$height_orig) {
                        $height_cropped = $width_orig * ($height/$width);
                        $top_y = ($height_orig - $height_cropped) / 2;
                        $bottom_y = $top_y + $height_cropped;
                        $top_x = 0;
                        $bottom_x = $width_orig;
                        $image->crop($top_x, $top_y, $bottom_x, $bottom_y);
                    } elseif ($width/$height < $width_orig/$height_orig) {
                        $width_cropped = $width * ($height_orig/$height);
                        $top_x = ($width_orig - $width_cropped) / 2;
                        $bottom_x = $top_x + $width_cropped;
                        $top_y = 0;
                        $bottom_y = $height_orig;
                        $image->crop($top_x, $top_y, $bottom_x, $bottom_y);
                    }
                }
                // ******
				$image->resize($width, $height);
				$image->save(DIR_IMAGE . $new_image);
			} else {
				copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
			}
		}

		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . 'images/' . $new_image;
		} else {
			return $this->config->get('config_url') . 'images/' . $new_image;
		}
	}
}
