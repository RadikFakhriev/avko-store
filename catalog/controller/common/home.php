<?php
class ControllerCommonHome extends Controller {

	public function phone_format($phone, $format, $mask = '#')
	{

		$phone = preg_replace('/[^0-9]/', '', $phone);

		if (is_array($format)) {
			if (array_key_exists(strlen($phone), $format)) {
				$format = $format[strlen($phone)];
			} else {
				return false;
			}
		}

		$pattern = '/' . str_repeat('([0-9])?', substr_count($format, $mask)) . '(.*)/';

		$format = preg_replace_callback(
			str_replace('#', $mask, '/([#])/'),
			function () use (&$counter) {
				return '${' . (++$counter) . '}';
			},
			$format
		);

		return ($phone) ? trim(preg_replace($pattern, $format, $phone, 1)) : false;
	}

	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$phone_formats = array(
			'11' => '#-###-###-##-##'
		);

		$data['formatted_telephone'] = $this->phone_format($this->config->get('config_telephone'), $phone_formats, '#');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}