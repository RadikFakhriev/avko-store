<?php

class ControllerFeedRestApi extends Controller {

	private $debugIt = false;
	
	/*
	* Get products
	*/
	public function products() {

		$this->checkPlugin();

		$this->load->model('catalog/product');
	
		$json = array('success' => true, 'products' => array());

		/*check category id parameter*/
		if (isset($this->request->get['category'])) {
			$category_id = $this->request->get['category'];
		} else {
			$category_id = 0;
		}

		$products = $this->model_catalog_product->getProducts(array(
			'filter_category_id'        => $category_id
		));

		foreach ($products as $product) {

			if ($product['image']) {
				$image = $product['image'];
			} else {
				$image = false;
			}

			if ((float)$product['special']) {
				$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}

			$json['products'][] = array(
					'id'			=> $product['product_id'],
					'name'			=> $product['name'],
					'description'	=> $product['description'],
					'pirce'			=> $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
					'href'			=> $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'thumb'			=> $image,
					'special'		=> $special,
					'rating'		=> $product['rating']
			);
		}

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		}
	}


	/*
	* Get products by ids
	*/
	public function getProductsByIds() {

		$this->checkPlugin();

		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		$json = array('success' => true, 'products' => array());

		if (isset($this->request->post['products'])) {
			$product_ids = $this->request->post['products'];
		} else {
			$product_ids = 0;
		}

		foreach ($product_ids as $product_id) {
			$products[] = $this->model_catalog_product->getProduct($product_id);
		}

		$category_tree_ids_mapper = function ($entry) {
			return $entry["category_id"];
		};

		foreach ($products as $product) {

			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], 480, 480);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 480, 480);
			}

			$categories_by_product = $this->model_catalog_product->getCategories($product['product_id']);
			$product_categories_ids = array_map($category_tree_ids_mapper, $categories_by_product);
			$product["category"] = $this->model_catalog_category->getCategory($product_categories_ids[2]);

			if ((float)$product['special']) {
				$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}

			$json['products'][] = array(
					'id'			=> $product['product_id'],
					'category'		=> "для " . $product['category']['name'],
					'name'			=> $product['name'],
					'description'	=> $product['description'],
					'price'			=> number_format(intval($this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')))), 0, '', ' ') ." Р",
					'href'          => htmlspecialchars_decode($this->url->link('product/category', 'path=' . $product["category"]['category_id'])),
					'thumb'			=> $image,
					'special'		=> $special,
					'rating'		=> $product['rating']
			);
		}

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		}
	}

	/*
	* Get products by manufacture
	*/
	public function getProductsByManufacture() {

		$this->checkPlugin();

		$this->load->model('catalog/product');

		$json = array('success' => true, 'products' => array());

		/*check category id parameter*/
		if (isset($this->request->get['manufacture'])) {
			$manufacture_id = $this->request->get['manufacture'];
		} else {
			$manufacture_id = 0;
		}

		$products = $this->model_catalog_product->getProducts(array(
				'filter_manufacturer_id' => $manufacture_id
		));

		foreach ($products as $product) {

			if ($product['image']) {
				$image = $product['image'];
			} else {
				$image = false;
			}

			if ((float)$product['special']) {
				$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}

			$json['products'][] = array(
					'id'			=> $product['product_id'],
					'name'			=> $product['name'],
					'description'	=> $product['description'],
					'pirce'			=> $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
					'href'			=> $this->url->link('product/product', 'product_id=' . $product['product_id']),
					'thumb'			=> $image,
					'special'		=> $special,
					'rating'		=> $product['rating']
			);
		}

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		}
	}

	/*
	* Get second level categories
	*/
	public function getCarModels () {

		$this->checkPlugin();
		$this->load->model('catalog/category');

		$json = array('success' => true, 'carModels' => array());

		/*check 1 level category id parameter*/
		if (isset($this->request->get['brand'])) {
			$parent_category_id = $this->request->get['brand'];
		} else {
			$parent_category_id = 0;
		}

		$data['categories'] = array();

		$children_categories = $this->model_catalog_category->getCategories($parent_category_id);

		$json['carModels'] = $children_categories;

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		}
	}


	public function requestFeedback() {
		$this->checkPlugin();
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$json = array('success' => true);

		$feedbackMailText = "";
		if(isset($_REQUEST["product"])) {
			$productId = html_entity_decode($this->request->post['product'], ENT_QUOTES, 'UTF-8');
		}

			if (isset($productId) && $productId != 0) {
				$product = $this->model_catalog_product->getProduct($productId);
				$choosedCategory = $this->model_catalog_product->getCategories($productId);
				$category = $this->model_catalog_category->getCategory($choosedCategory[2]['category_id']);
				$feedbackMailText = "Клиент с номером телефона: <br/>"
						. $this->request->post['enquiry']
						. "<br/>Интересуемый товар: <br/><b>"
						. $product['name']
						. "</b><br/> Артикул: <br/><b>"
						. $product['model']
						. "</b><br/> на автомобиль:<br/><b>"
						. $category['name'] . "</b><br/>";
		} else {
			$feedbackMailText = "Клиент с номером телефона: <br/>"
					. $this->request->post['enquiry'];
		}

		$feedbackMailText .= "<br/></br></br>---Сообщение сгенерировано ботом, не нужно отвечать на него--- ";

		if (!isset($this->request->post['feedback'])) {
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->request->post['email']);
			$mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->request->post['subject'], ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($feedbackMailText);
			$mail->send();
		}

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		}

	}

	protected function validateFeddback() {
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['enquiry']) < 10) || (utf8_strlen($this->request->post['enquiry']) > 3000)) {
			$this->error['enquiry'] = $this->language->get('error_enquiry');
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('contact', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		return !$this->error;
	}


	/*
	* Get orders
	*/
	public function orders() {

		$this->checkPlugin();
	
		$orderData['orders'] = array();

		$this->load->model('account/order');

		/*check offset parameter*/
		if (isset($this->request->get['offset']) && $this->request->get['offset'] != "" && ctype_digit($this->request->get['offset'])) {
			$offset = $this->request->get['offset'];
		} else {
			$offset 	= 0;
		}

		/*check limit parameter*/
		if (isset($this->request->get['limit']) && $this->request->get['limit'] != "" && ctype_digit($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit 	= 10000;
		}
		
		/*get all orders of user*/
		$results = $this->model_account_order->getAllOrders($offset, $limit);
		
		$orders = array();

		if(count($results)){
			foreach ($results as $result) {

				$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
				$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

				$orders[] = array(
						'order_id'		=> $result['order_id'],
						'name'			=> $result['firstname'] . ' ' . $result['lastname'],
						'status'		=> $result['status'],
						'date_added'	=> $result['date_added'],
						'products'		=> ($product_total + $voucher_total),
						'total'			=> $result['total'],
						'currency_code'	=> $result['currency_code'],
						'currency_value'=> $result['currency_value'],
				);
			}

			$json['success'] 	= true;
			$json['orders'] 	= $orders;
		}else {
			$json['success'] 	= false;
		}
		
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';

		} else {
			$this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		}
	}	
	
	
	private function checkPlugin() {

		$json = array("success"=>false);

		/*check rest api is enabled*/
		if (!$this->config->get('rest_api_status')) {
			$json["error"] = 'API is disabled. Enable it!';
		}
		
		/*validate api security key*/
		if ($this->config->get('rest_api_key') && (!isset($this->request->get['key']) || $this->request->get['key'] != $this->config->get('rest_api_key'))) {
			$json["error"] = 'Invalid secret key';
		}
		
		if(isset($json["error"])){
			$this->response->addHeader('Content-Type: application/json');
			echo(json_encode($json));
			exit;
		}else {
			$this->response->setOutput(json_encode($json));
		}	
	}	

}
