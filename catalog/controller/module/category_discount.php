<?php
class ControllerModuleCategoryDiscount extends Controller {
    public function index($setting) {
        $this->load->language('module/category_discount');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');


        $discounts_list = $setting['discounts'];
        $data['discounts_list'] = json_encode($discounts_list);

        if (count($discounts_list)) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category_discount.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/category_discount.tpl', $data);
            } else {
                return $this->load->view('default/template/module/category_discount.tpl', $data);
            }
        }
    }
}