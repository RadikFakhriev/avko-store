<?php
class ControllerModuleGmapGeoAddress extends Controller {
    public function index() {
        $this->load->language('module/gmap_geo_address');
         
        //$data['heading_title'] = $this->language->get('heading_title'); // set the heading_title of the module
        
        $data['entry_map_address'] = $this->language->get('entry_map_address');
        $data['entry_map_geo_address'] = $this->language->get('entry_map_geo_address');
        
        /* Get Map Address Details */
        $data['gmap_geo_address'] = $this->config->get('gmap_geo_address_map_address');

        /* Add Neccessary Javascript and CSS files */
        $this->document->addStyle('catalog/view/javascript/jquery/gmap-geocomplete/jquery.geocomplete.css');
         
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/gmap_geo_address.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/gmap_geo_address.tpl', $data);
        } else {
            return $this->load->view('default/template/module/gmap_geo_address.tpl', $data);
        }
    }
    
}

