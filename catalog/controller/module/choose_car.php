<?php
/**
 * Created by IntelliJ IDEA.
 * User: Радик
 * Date: 11.11.2015
 * Time: 15:26
 */

class ControllerModuleChooseCar extends Controller{

    public function index($setting) {
        static $module = 0;

        $this->load->model('tool/image');
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    $filter_data = array(
                        'filter_category_id'  => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $category['static_image'] = 'brands/' . mb_strtolower($this->translitText($child['name']), 'UTF-8') . '.png';
                    if(is_file(DIR_IMAGE . $category['static_image'])) {
                        $children_data[] = array(
                            'id'    => $child['category_id'],
                            'name'  => $child['name'],
                            'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                            'image' => $this->model_tool_image->resize($category['static_image'], $setting['width'], $setting['height'], false)
                        );
                    }

                }

                $data['categories'] = $children_data;

//                if (is_file($category['static_image'])) {
//                    // Level 1
//                    $data['categories'][] = array(
//                        'id'       => $category['category_id'],
//                        'name'     => $category['name'],
//                        'children' => $children_data,
//                        'column'   => $category['column'] ? $category['column'] : 1,
//                        'href'     => $this->url->link('product/category', 'path=' . $category['category_id']),
//                        'image' => $this->model_tool_image->resize($category['static_image'], $setting['width'], $setting['height'])
//                    );
//                }

            }
        }

        $data['module'] = $module++;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/choose_car.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/choose_car.tpl', $data);
        } else {
            return $this->load->view('default/template/module/slideshow.tpl', $data);
        }
    }

    private function translitText($str)
    {
        $tr = array(
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
            "Д"=>"D","Е"=>"E","Ё"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
            "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
            "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
        );
        return strtr($str,$tr);
    }

}